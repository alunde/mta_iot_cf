See the Evernote titled.

CF with MTA Plugin on Canary


Must be VPN'd into WDF and run these env vars.

$ cat cf_wdf_proxy
#!/bin/bash
```
export http_proxy=http://proxy.wdf.sap.corp:8080
export HTTP_PROXY=http://proxy.wdf.sap.corp:8080
export https_proxy=http://proxy.wdf.sap.corp:8080
export HTTPS_PROXY=http://proxy.wdf.sap.corp:8080
export ftp_proxy=http://proxy.wdf.sap.corp:8080
export FTP_PROXY=http://proxy.wdf.sap.corp:8080
export all_proxy=http://proxy.wdf.sap.corp:8080
export ALL_PROXY=http://proxy.wdf.sap.corp:8080
export no_proxy=*.local,169.254/16,*.sap.corp,*.corp.sap,.local,.sap.corp,.corp.sap,*.cloud.sap,.cloud.sap
export NO_PROXY=*.local,169.254/16,*.sap.corp,*.corp.sap,.local,.sap.corp,.corp.sap,*.cloud.sap,.cloud.sap
```

cf api https://api.cf.sap.hana.ondemand.com

$ cf api
API endpoint:   https://api.cf.sap.hana.ondemand.com
API version:    2.65.0

cf login -u I830671
pw: SAP-ID pw

You must bring NodeJS modules local by changing the npm registry to the internal one.

http://nexus.wdf.sap.corp:8081/nexus/content/groups/build.milestones.npm/#browse/browse/assets

$ npm config get registry
http://nexus.wdf.sap.corp:8081/nexus/content/groups/build.milestones.npm/

npm config set registry http://nexus.wdf.sap.corp:8081/nexus/content/groups/build.milestones.npm/
npm config set registry http://registry.npmjs.org/

//Build with MTA builder.

cd /Users/i830671/mta_iot
java -jar ../mta-cli-1.0.1.jar --build-target=XSA --mtar=../mta_iot.mtar build
java -jar ../mta-cli-1.0.1.jar --build-target=CF --mtar=../mta_iot.mtar build


// Deploy with MTA plugin installed

cf deploy